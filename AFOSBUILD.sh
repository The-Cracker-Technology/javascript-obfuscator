rm -rf /opt/ANDRAX/javascript-obfuscator

WORKDIR=$(pwd)

cp -Rf $(pwd) /opt/ANDRAX/javascript-obfuscator

cd /opt/ANDRAX/javascript-obfuscator

rm -rf andraxbin/

yarn

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Yarn... PASS!"
else
  # houston we have a problem
  exit 1
fi

yarn run webpack --config ./webpack/webpack.node.config.js --config ./webpack/webpack.browser.config.js --mode production

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Yarn run... PASS!"
else
  # houston we have a problem
  exit 1
fi

cd $WORKDIR

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
